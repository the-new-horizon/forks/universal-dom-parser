if (typeof window !== "undefined") {
  if (
    typeof navigator === "undefined" ||
    !(
      navigator.userAgent.includes("Node.js") ||
      navigator.userAgent.includes("jsdom")
    )
  ) {
    throw new Error(
      "The window object is availible. Are you trying to run the module in the Browser?"
    );
  }
}

const { JSDOM } = require("jsdom");
const DOMParser = new JSDOM().window.DOMParser;

module.exports = DOMParser;
